#pragma once
#include "De.h"


class Plateau {
  De *des;
  bool bloques[3];
  public:
    Plateau();
    void tourneDes();
    int getValeur(int numDe);
    void bloquerDe(int numDe);
    De* getDes();
    int nbPoints();
};